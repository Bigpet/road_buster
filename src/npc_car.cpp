#include "npc_car.h"

#include "rb_platform.h"
#include "rb_renderer.h"
#include "player_car.h"
#include "play_state.h"
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <iterator>


#include <cstdlib>
#include <climits>

#include <iostream>

struct npc_car_animation
{
	int millis_per_frame;
	std::vector<RB_rect> frames;
};

#define NUM_CAR_TYPES 12
enum npc_car_types : int
{
	SPORTY=0,
	RACECAR=1,
	MINI_TRUCK=2,
	MINI_BUS=3,
	TRUCK=4,
	BUS=5,
	SPORTY_R=6,
	RACECAR_R=7,
	MINI_TRUCK_R=8,
	MINI_BUS_R=9,
	TRUCK_R=10,
	BUS_R=11,
};

#define SPR_H 70
static std::map<npc_car_types, npc_car_animation> npc_animation_map{
	{ SPORTY, { 300, { { 0, 0, 34, 34 }, { 34, 0, 34, 34 } } } },
	{ RACECAR, { 200, { { 0, 35, 34, 34 }, { 34, 35, 34, 34 } } } },
	{ MINI_TRUCK, { 300, { { 2*34, 0, 34, 34 }, { 3*34, 0, 34, 34 } } } },
	{ MINI_BUS, { 300, { { 2 * 34, 35, 34, 34 }, { 3 * 34, 35, 34, 34 } } } },
	{ TRUCK, { 300, { { 4*34, 0, 34, 64 }, { 5*34, 0, 34, 64 } } } },
	{ BUS, { 300, { { 6*34, 0, 34, 64 }, { 7*34, 0, 34, 64 } } } },
	{ SPORTY_R, { 300, { { 8 * 34, SPR_H - 34, 34, 34 }, { 9 * 34, SPR_H- 34, 34, 34 } } } },
	{ RACECAR_R, { 200, { { 8 * 34, SPR_H - (35+34), 34, 34 }, { 9*34, SPR_H - (35+34), 34, 34 } } } },
	{ MINI_TRUCK_R, { 300, { { 10 * 34, SPR_H-34, 34, 34 }, { 11 * 34, SPR_H - 34, 34, 34 } } } },
	{ MINI_BUS_R, { 300, { { 10 * 34, SPR_H - (35+34), 34, 34 }, { 11 * 34, SPR_H-(35+34), 34, 34 } } } },
	{ TRUCK_R, { 300, { { 12 * 34, SPR_H-64, 34, 64 }, { 13 * 34, SPR_H - 64, 34, 64 } } } },
	{ BUS_R, { 300, { { 14 * 34, SPR_H - 64, 34, 64 }, { 15 * 34, SPR_H-64, 34, 64 } } } }

};
#undef SPR_H

struct NPC_state
{
	enum state
	{
		NORMAL, BOUNCING,EVADING,HALTED
	};
	NPC_state::state behavior;
	RB_sprite sprite;
	int progress;
	int speed;
	int max_speed;
	bool hit;
	int getYOff()
	{
		return 0;
	}
	npc_car_types type;
};

std::vector<NPC_state> left_lane;
std::vector<NPC_state> right_lane;

static struct NPC_comparator
{
	bool operator ()(const NPC_state& a, const NPC_state& b) const
	{
		return a.progress < b.progress;
	}
} npc_comp;

//std::set<NPC_state,  NPC_comparator> cars_on_road;

Npc_car::Npc_car() :
cars(NPC_CARS01),
progress(0),
millis_total(0),
generated_up_to(0)
{

}

void Npc_car::draw(RB_renderer &surf, int x_off, int y_off)
{
	NPC_state dummy;
	dummy.progress = progress + 600 * UNIT_PER_PIXEL;
	//auto bound = cars_on_road.upper_bound(dummy);
	auto bound_left = std::upper_bound(left_lane.begin(), left_lane.end(), dummy, npc_comp);
	auto bound_right = std::upper_bound(right_lane.begin(), right_lane.end(), dummy, npc_comp);

	//std::for_each(cars_on_road.begin(), bound, [&](const NPC_state& to_draw)
	auto lambda = [&](NPC_state& to_draw)
	{
		int loc_y = ((int) progress - (int) to_draw.progress) / UNIT_PER_PIXEL;
		//if (loc_y < 0)
		//{
		//	local_cpy.pos.y = 0;
		//	local_cpy.clip.h += loc_y;
		//	local_cpy.clip.y -= loc_y;
		//}
		//else{
		to_draw.sprite.pos.y = loc_y;
		//local_cpy.pos.y = loc_y;
		//}
		//local_cpy.pos.h = local_cpy.clip.h;
		//local_cpy.pos.w = local_cpy.clip.w;
		//local_cpy.draw(surf);
		to_draw.sprite.draw(surf,x_off,y_off);
	};
	std::for_each(left_lane.begin(), bound_left, lambda);
	std::for_each(right_lane.begin(), bound_right, lambda);
	//RB_sprite local_cpy = cars;
	//local_cpy.clip = animation_map[BUS].frames.front();
	//local_cpy.pos.x = 150;
	//local_cpy.pos.y = progress % 320;
	//local_cpy.pos.w = local_cpy.clip.w;
	//local_cpy.pos.h = local_cpy.clip.h;

	//local_cpy.draw(surf);
}

void Npc_car::generate_cars()
{
	int pos = progress / UNIT_PER_PIXEL;
	if (pos + 400 > generated_up_to)
	{
		int start_gen = pos + 400;
		for (int gen_cnt = start_gen; gen_cnt < pos + 5000; gen_cnt += 70)
		{
			int place = rand();
			if (place % 100 > 70)
			{
				NPC_state new_car;
				new_car.behavior = NPC_state::NORMAL;
				new_car.sprite = cars;
				new_car.progress = gen_cnt * UNIT_PER_PIXEL;
				new_car.type = (npc_car_types) (rand() % NUM_CAR_TYPES / 2);
				new_car.sprite.clip = npc_animation_map[new_car.type].frames.front();
				new_car.sprite.pos = new_car.sprite.clip;
				int lane = rand() % 4;
				int lane_width = max_x - min_x;
				int one_lane = lane_width / 4;
				new_car.sprite.pos.x = max_x - 30 - lane*one_lane ;//267- 72 - (lane) * 60;
				new_car.sprite.pos.y = -200;
				new_car.speed = 100 + (rand() % 100);
				new_car.max_speed = new_car.speed;
				new_car.hit = false;
				if (lane > 1)
				{
					new_car.speed *= -1;
					new_car.max_speed *= -1;
					new_car.type = (npc_car_types) (((int) new_car.type) + NUM_CAR_TYPES / 2);
					//auto ins_pos = std::lower_bound(left_lane.begin(), left_lane.end(), new_car, npc_comp);
					//left_lane.insert(ins_pos,new_car);
					new_car.sprite.clip = npc_animation_map[new_car.type].frames.front();
					left_lane.push_back(new_car);
				}
				else
				{
					//auto ins_pos = std::lower_bound(right_lane.begin(), right_lane.end(), new_car, npc_comp);
					//right_lane.insert(ins_pos, new_car);
					right_lane.push_back(new_car);
				}
				//cars_on_road.insert(new_car);
			}
		}
		generated_up_to = pos + 5000;
	}
}


template<typename T, typename T2>
int check_collision(T& car1, T2& car2)
{
	if (
		car2.progress < (int) car1.progress - car1.getYOff() * UNIT_PER_PIXEL + ((car2.sprite.pos.h - 4) * UNIT_PER_PIXEL) &&
		car2.progress > (int) car1.progress - car1.getYOff() * UNIT_PER_PIXEL - ((car1.sprite.pos.h - 4) * UNIT_PER_PIXEL) &&
		car2.sprite.pos.x < car1.sprite.pos.x + car2.sprite.pos.w - 6 &&
		car2.sprite.pos.x > car1.sprite.pos.x - car2.sprite.pos.w + 6
		)
	{
		int speed_hit = std::min(car1.speed, car2.speed);
		return speed_hit>0 ? speed_hit : 1 ;
	}
	else
	{
		return 0;
	}
}

template<typename T>
int move_cars(T start_it, T end_it, Player_car &player_car, int progress, int posy, int posx, int millis_since_last_step, int min_x, int max_x)
{
	int speed_hit = 0;
	std::for_each(start_it, end_it, [&](NPC_state &car_state){
		switch (car_state.behavior)
		{
		case NPC_state::NORMAL:
		{
			if (
				car_state.progress < (int) progress - (190 + posy) * UNIT_PER_PIXEL + ((car_state.sprite.pos.h - 4) * UNIT_PER_PIXEL) &&
				car_state.progress >(int)progress - (190 + posy) * UNIT_PER_PIXEL - (32 * UNIT_PER_PIXEL) &&
				car_state.sprite.pos.x < posx + car_state.sprite.pos.w - 6 &&
				car_state.sprite.pos.x > posx - car_state.sprite.pos.w + 6
				)
			{
				car_state.hit = true;
				speed_hit = speed_hit ? std::min(speed_hit, car_state.speed) : car_state.speed;
			}
			int collision = check_collision(player_car, car_state);
			if (car_state.hit)
			{
				car_state.behavior = NPC_state::BOUNCING;
				car_state.speed = std::abs(car_state.speed);
				car_state.progress += 5 * UNIT_PER_PIXEL;
			}
		}
			break;
		case NPC_state::BOUNCING:
			if (car_state.speed > 100)
			{
				car_state.speed += -5*millis_since_last_step;
			}
			else
			{
				car_state.behavior = NPC_state::EVADING;
			}
			break;
		case NPC_state::EVADING:
			if (car_state.sprite.pos.x < (max_x-min_x)/2 + min_x) // left lane
			{
				//TODO: (Peter): make relative to milliseconds
				if (car_state.speed > -100) car_state.speed -= 5;
				car_state.sprite.pos.x -= 1;
				if (car_state.sprite.pos.x < min_x) car_state.behavior = NPC_state::HALTED;
			}
			else
			{
				if (car_state.speed < 100) car_state.speed += 5;
				car_state.sprite.pos.x += 1;
				if (car_state.sprite.pos.x > max_x) car_state.behavior = NPC_state::HALTED;
			}
			break;
		case NPC_state::HALTED:
			car_state.speed *= 0.9f;
			break;
		}
		car_state.progress += millis_since_last_step * car_state.speed;

	});
	return speed_hit;

}

int Npc_car::step(int millis_since_last_step, int pos, Player_car &player_car, int posy, int posx)
{
	millis_total += millis_since_last_step;
	progress = pos;

	//erase everything about 1000px behind us
	NPC_state dummy;
	dummy.progress = (int) progress - 1000 * UNIT_PER_PIXEL;
	auto lower_bound_left = std::lower_bound(left_lane.begin(), left_lane.end(), dummy, npc_comp);
	left_lane.erase(left_lane.begin(),lower_bound_left);
	auto lower_bound_right = std::lower_bound(right_lane.begin(), right_lane.end(), dummy, npc_comp);
	right_lane.erase(right_lane.begin(), lower_bound_right);

	//move everything from 400 behind us to about 500 in front of us
	dummy.progress = (int) progress - 400 * UNIT_PER_PIXEL;
	lower_bound_left = std::lower_bound(left_lane.begin(), left_lane.end(), dummy, npc_comp);
	lower_bound_right = std::lower_bound(right_lane.begin(), right_lane.end(), dummy, npc_comp);

	dummy.progress = (int) progress + 500 * UNIT_PER_PIXEL;
	auto upper_bound_left = std::upper_bound(lower_bound_left, left_lane.end(), dummy, npc_comp);
	auto upper_bound_right = std::upper_bound(lower_bound_right, right_lane.end(), dummy, npc_comp);

	int hit_left = move_cars(lower_bound_left, upper_bound_left, player_car, progress, posy, posx, millis_since_last_step , min_x, max_x);
	int hit_right = move_cars(lower_bound_right, upper_bound_right, player_car, progress, posy, posx, millis_since_last_step, min_x, max_x);

		//NPC_state dummy;
		//dummy.progress = (int)progress - 1000*UNIT_PER_PIXEL;
		//auto bound = cars_on_road.lower_bound(dummy);
		//erase cars that are 1000 units behind us
		//cars_on_road.erase(cars_on_road.begin(), bound);

		//move all cars in the range [400 behind us --- 500 in front)
		//dummy.progress = (int) progress - 400 * UNIT_PER_PIXEL;
		//bound = cars_on_road.lower_bound(dummy);
		//dummy.progress = (int) progress + 500 * UNIT_PER_PIXEL;
		//auto upper_bound = cars_on_road.upper_bound(dummy);

		//std::vector<NPC_state> moving_cars;
		//std::copy(bound, upper_bound, std::back_inserter(moving_cars));
		//cars_on_road.erase(bound, upper_bound);

		//for (auto &car_state : moving_cars)
		//{
		//	if (
		//		car_state.progress < (int)progress - (190 +posy) * UNIT_PER_PIXEL + (34 * UNIT_PER_PIXEL) &&
		//		car_state.progress > (int)progress -(190 + posy)*UNIT_PER_PIXEL - (34 * UNIT_PER_PIXEL) &&
		//		car_state.lane < posx+32  &&
		//		car_state.lane > posx-32 
		//		)
		//	{
		//		car_state.hit = true;
		//	}
		//	int advancement = millis_since_last_step * car_state.speed;
		//	if (car_state.hit)
		//	{
		//		//car_state.progress -= advancement;
		//	}
		//	else
		//	{
		//		car_state.progress += advancement;
		//	}
		//}

		//std::copy(moving_cars.begin(), moving_cars.end(), std::inserter(cars_on_road,cars_on_road.begin()));

		generate_cars();
		std::sort(left_lane.begin(), left_lane.end(),npc_comp);
		std::sort(right_lane.begin(), right_lane.end(), npc_comp);
		if (hit_right)
		{
			return hit_right;
		}
		else
		{
			return hit_left;
		}
}

Npc_car *Npc_car::get_random_car()
{
	return nullptr;
}

