#pragma once

struct RB_rect
{
	int x, y;
	int w, h;
};

struct RB_renderer_state;
struct RB_texture;
struct RB_renderer
{
	RB_renderer_state *state;

	void clear();
	void draw_texture(RB_texture &tex, RB_rect *source, RB_rect *target);
	void present();
};
RB_renderer *get_renderer();