#pragma once

#include <string>
#include <memory>
#include <chrono>

#include "Main_menu.h"
#include "rb_platform.h"

struct SDL_Window;
struct SDL_renderer;

using sys_clock = std::chrono::high_resolution_clock;

struct Road_buster_game{
	//const char *COMPANY = "GameDanTeam";
	//const char *APPLICATION = "ROAD BUSTER";

	////TODO: (Peter): read settings from config file 
	//const int DEFAULT_WIDTH = 480;
	//const int DEFAULT_HEIGHT = 640;

	RBp_settings *settings;
	RB_renderer *rend;

	sys_clock::time_point last_frame;
	int millis_total;
	int millis_since_last_update;
	//std::string game_path;

	bool should_quit = false;

	//TODO: (Peter) remove
	std::unique_ptr<Game_state> current_state;

	bool init();

	void loop();

	void cleanup();
};

extern Road_buster_game GAME;