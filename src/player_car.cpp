#include "player_car.h"
#include "play_state.h"

#include <algorithm>
#include <vector>
#include <map>

//TODO: (Peter): generalize animated sprite with multiple animations
struct RB_vec2
{
	int x, y;
};
struct car_animation
{
	int millis_per_frame;
	std::vector<RB_vec2> frames;
};

enum car_anim_state
{
	STANDING_STILL,
	MOVING,
	ACCELERATING_SLOW,
	ACCELERATING_FAST,
	ACCELERATING_BOOST,
	ACCELERATING_LEFT,
	ACCELERATING_RIGHT,
};

static std::map<car_anim_state, car_animation> animation_map{
	{ STANDING_STILL, { { 500 }, { { 0, 0 } } } },
	{ MOVING, { { 150 }, { { 0, 0 }, { 0, 1 } } } },
	{ ACCELERATING_SLOW, { { 150 }, { { 0, 1 }, { 1, 1 }, { 2, 1 } } } },
	{ ACCELERATING_FAST, { { 100 }, { { 1, 0 }, { 2, 0 } } } },
	{ ACCELERATING_BOOST, { { 50 }, { { 3, 1 }, { 4, 1 } } } },
	{ ACCELERATING_LEFT, { { 100 }, { { 3, 0 }, { 4, 0 } } } },
	{ ACCELERATING_RIGHT, { { 100 }, { { 5, 0 }, { 6, 0 } } } }
};

static car_anim_state cur_state = STANDING_STILL;

Player_car::Player_car() :
speed(0),
acceleration(0),
max_acceleration(20),
max_speed(10000),
boost(0),
progress(0),
boost_acceleration(0),
boost_remaining(5000000),
boost_full_at(5000000),
sprite(PLAYER_CAR),
input(),
car_y_offset(),
millis_total(0),
car_x_pos(120 * UNIT_PER_PIXEL)
{
	sprite.pos.x = 120;
	sprite.pos.y = 190;
	sprite.pos.w = 34;
	sprite.pos.h = 48;
	sprite.clip.w = 34;
	sprite.clip.h = 48;

}

void Player_car::draw(RB_renderer &surf, int x_off, int y_off)
{
	car_animation anim = animation_map[cur_state];
	int frame = millis_total % (anim.frames.size() * anim.millis_per_frame);
	frame = frame / anim.millis_per_frame;
	RB_vec2 sprite_pos = anim.frames[frame];
	sprite.clip.x = sprite.clip.w * sprite_pos.x;
	sprite.clip.y = sprite.clip.h * sprite_pos.y;
	sprite.draw(surf,x_off,y_off + car_y_offset);
}

void Player_car::step(int millis_since_last_step)
{
	if (input[UP] || input[BOOST])
	{
		acceleration = std::max(acceleration + 2, max_acceleration);
		speed = std::min(speed + acceleration, max_speed);
		if (speed < max_speed / 2) cur_state = ACCELERATING_SLOW;
		else cur_state = ACCELERATING_FAST;

		if (input[BOOST] && boost_remaining)
		{
			boost_acceleration = std::max(boost_acceleration + 4, max_acceleration * 2);
			boost = std::min(boost + boost_acceleration, max_speed * 3 / 2);
			boost_remaining = std::max(boost_remaining - boost,0);
			cur_state = ACCELERATING_BOOST;
		}
		else
		{
			boost_acceleration *= 0.8;
			boost *= 0.9;
		}
	}
	else if (input[DOWN])
	{
		speed = std::max(speed - 250, 0);
		acceleration /= 4;
		boost_acceleration *= 0.7;
		boost *= 0.8;
		if (speed > 10) cur_state = STANDING_STILL;
	}
	else
	{
		boost_acceleration *= 0.8;
		boost *= 0.9;
		acceleration *= 0.9f;
		if (speed > max_speed / 2)
		{
			speed *= 0.995f;
		}
		else
		{
			speed *= 0.99f;
		}

		if (speed > max_speed / 50) cur_state = MOVING;
		else cur_state = STANDING_STILL;
	}
	if (input[LEFT] && speed>0)
	{
		int more_x = millis_since_last_step * speed / 30;
		car_x_pos -= more_x;
		car_x_pos = std::max(car_x_pos, car_min_x_pos * UNIT_PER_PIXEL);
		sprite.pos.x = car_x_pos / UNIT_PER_PIXEL;
		if (input[UP]) cur_state = ACCELERATING_LEFT;
	}
	else if (input[RIGHT] && speed>0)
	{
		int more_x = millis_since_last_step * speed / 30;
		car_x_pos += more_x;
		car_x_pos = std::min(car_x_pos, car_max_x_pos * UNIT_PER_PIXEL);
		sprite.pos.x = car_x_pos / UNIT_PER_PIXEL;
		if (input[UP]) cur_state = ACCELERATING_RIGHT;
	}
	car_y_offset = 60.0f * (speed + 0.5f*boost) / max_speed;
	millis_total += millis_since_last_step;
}

int Player_car::getYOff()
{
	return 190 + car_y_offset;
}
