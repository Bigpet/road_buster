#pragma once

#include "rb_sprite.h"
#include "game_state.h"
#include "player_car.h"
#include "npc_car.h"
#include "level_timer.h"

#define UNIT_PER_PIXEL 1600

struct RB_event;
struct RB_renderer;
struct Game_state;

struct Play_state : Game_state
{
	Play_state();
	virtual void process_events(RB_event &evt);
	virtual Game_state* step(int millis_since_last_step) ;
	virtual void draw(RB_renderer &surf) ;
	virtual ~Play_state();

private:
	Player_car player;


	int progress_goal;


	int millis_total;

	int change_state;

	int progress_bar_min;
	int progress_bar_max;

	Npc_car enemies;
	Level_timer timer;
	RB_sprite goal_bar;
	RB_sprite boost_bar;
	RB_sprite boost_bar_remaining;
	RB_sprite progress_indicator;
	RB_sprite road[14];
};