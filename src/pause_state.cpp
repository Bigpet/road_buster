//std::unique_ptr<Game_state> backdrop;
//bool forward_backdrop;
//RB_sprite pause;

#include "pause_state.h"
#include "rb_event.h"

Pause_state::Pause_state() :
forward_backdrop(false),
pause(PAUSE_TEX),
exit_next(false)
{

}

void Pause_state::process_events(RB_event &evt)
{
	if (evt.type == RB_event::ACTION)
	{
		switch (evt.action)
		{
		case RB_event::ESCAPE:
		{
			if (evt.act_type == RB_event::START)
			exit_next = true;
			break;
		}
		}
	}
}
Game_state* Pause_state::step(int millis_since_last_step)
{
	if (forward_backdrop && backdrop)
	{
		backdrop->step(millis_since_last_step);
	}
	if (exit_next)
	{
		exit_next = false;
		return backdrop.release();
	}
	else
	{
		return this;
	}
}


void Pause_state::draw(RB_renderer &surf)
{
	surf.clear();
	if (backdrop)
	{
		backdrop->draw(surf);
	}
	pause.pos.x = 70;
	pause.pos.y = 30;
	pause.draw(surf);
}
