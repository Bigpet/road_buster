#include "road_buster.h"

#ifdef USE_SDL
#include "SDL.h" //for SDLmain defines
#endif

//TODO: (Peter): make Emscripten a completely distinct platform implementation from SDL
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <html5.h>

#define EMK_LEFT   37
#define EMK_UP     38
#define EMK_RIGHT  39
#define EMK_DOWN   40
#define EMK_SPACE  32
#define EMK_RETURN 13
#define EMK_ESCAPE 27
#define EMK_Z      90
#define EMK_Y      89
#define EMK_X      88
#define EMK_C      67

void loop_callback()
{
	GAME.loop();
}
#include "rb_event.h"
EM_BOOL em_key_down(int eventType, const EmscriptenKeyboardEvent *keyEvent, void *userData)
{
	switch(eventType)
	{
	case EMSCRIPTEN_EVENT_KEYPRESS:
		//printf("key_pressed key: %s \n",keyEvent->key);
		//printf("key_pressed code: %s \n",keyEvent->code);
		//printf("key_pressed which: %d \n",keyEvent->which);
		break;
	case EMSCRIPTEN_EVENT_KEYDOWN:
		//printf("key_down key: %s \n",keyEvent->key);
		//printf("key_down code: %s \n",keyEvent->code);
		//printf("key_down which: %d \n",keyEvent->which);
		break;
	case EMSCRIPTEN_EVENT_KEYUP:
		//printf("key_up key: %s \n",keyEvent->key);
		//printf("key_up code: %s \n",keyEvent->code);
		//printf("key_up which: %d \n",keyEvent->which);
		break;
	}
		RB_event translation;
		bool forward = false;
	
		switch (eventType)
		{
		case EMSCRIPTEN_EVENT_KEYDOWN:
		case EMSCRIPTEN_EVENT_KEYUP:
		{
			translation.type = RB_event::ACTION;
			translation.act_type = (eventType == EMSCRIPTEN_EVENT_KEYDOWN ? RB_event::START : RB_event::STOP);
			switch (keyEvent->which)
			{
			case EMK_UP:
				translation.action = RB_event::UP;
				forward = true;
				break;
			case EMK_DOWN:
				translation.action = RB_event::DOWN;
				forward = true;
				break;
			case EMK_LEFT:
				translation.action = RB_event::LEFT;
				forward = true;
				break;
			case EMK_RIGHT:
				translation.action = RB_event::RIGHT;
				forward = true;
				break;
			case EMK_Z:
			case EMK_Y:
				translation.action = RB_event::Z;
				forward = true;
				break;
			case EMK_X:
				translation.action = RB_event::X;
				forward = true;
				break;
			case EMK_C:
				translation.action = RB_event::C;
				forward = true;
				break;
			case EMK_SPACE:
				translation.action = RB_event::SPACE;
				forward = true;
				break;
			case EMK_RETURN:
				translation.action = RB_event::RETURN;
				forward = true;
				break;
			case EMK_ESCAPE:
				translation.action = RB_event::ESCAPE;
				forward = true;
				break;
			}
		}
		break;
		}
	
		if(forward && GAME.current_state)
		{
			GAME.current_state->process_events(translation);
			return true;
		}
		else
		{
			return false;
		}
}
#endif

int main(int argc, char **argv)
{

	if (!GAME.init())
	{
		return 1;
	}

#ifdef __EMSCRIPTEN__

	emscripten_set_keyup_callback("#document", nullptr, false, em_key_down);
	emscripten_set_keydown_callback("#document", nullptr, true, em_key_down);
	emscripten_set_keypress_callback("#document", nullptr, false, em_key_down);
	emscripten_set_main_loop(loop_callback, 0, 1);
#else

	::main_loop([](){GAME.loop(); });
	GAME.cleanup();
#endif
	return(0);
}
