// road_buster.cpp : Defines the entry point for the application.
//

#include "rb_platform.h"
#include "road_buster.h"
#include "rb_renderer.h"
#include "rb_texture.h"
#include "rb_event.h"

#include "SDL.h"

#include <iostream>

#include <chrono>
#include <thread>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif


Road_buster_game GAME;


bool Road_buster_game::init()
{
	last_frame = sys_clock::now();
	millis_total = 0;
	millis_since_last_update = 16;

	settings = create_Settings();
	::init(settings);

	if (!current_state)
	{
		current_state.reset(new Main_menu_state());
	}

	rend = ::get_renderer();

	return true;

}

void Road_buster_game::loop()
{
	millis_total += millis_since_last_update;
	auto now = sys_clock::now();
	auto diff = now - last_frame;
	millis_since_last_update = std::chrono::duration_cast<std::chrono::milliseconds>(diff).count();
	last_frame = now;

	std::this_thread::sleep_for(std::chrono::milliseconds(1));
	//Now lets draw our image
	Game_state *next_state = current_state->step(millis_since_last_update);
	if (next_state != current_state.get())
	{
		current_state.reset(next_state);
	}
	current_state->draw(*rend);
	rend->present();
}

void Road_buster_game::cleanup()
{
	shutdown(nullptr);
}

