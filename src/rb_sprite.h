#pragma once
#include "rb_renderer.h"
#include "rb_texture.h"
#include <memory>

struct RB_sprite
{
	RB_rect pos;
	RB_rect clip;
	std::shared_ptr<RB_texture> tex;

	RB_sprite();
	RB_sprite(enTexture);
	void setTexture(enTexture);

	void draw(RB_renderer &rnd, int x_off, int y_off);
	void draw(RB_renderer &rnd);
};