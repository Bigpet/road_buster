#pragma once

#include <string>
#include <functional>
//TODO: (Peter): stub


//Initialization and shutdown
struct RBp_settings;

RBp_settings *create_Settings();

void readSettings(RBp_settings *settings);

void init(RBp_settings *settings);

void shutdown(RBp_settings *settings);


//Utility 
void show_fatal_error(const std::string &msg);

std::string translate_directory( const std::string &relative_path);

//Rendering


//entry point into loop

void main_loop(std::function<void(void)> callback);