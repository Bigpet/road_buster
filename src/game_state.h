#pragma once

struct RB_event;
struct RB_renderer;

struct Game_state
{
	virtual void process_events(RB_event &evt) = 0;
	virtual Game_state* step(int millis_since_last_step) = 0;
	virtual void draw(RB_renderer &surf) = 0;
	virtual ~Game_state(){}
};