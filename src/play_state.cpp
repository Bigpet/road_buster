#include "play_state.h"
#include "game_state.h"
#include "main_menu.h"
#include "rb_event.h"
#include "pause_state.h"
#include "road_buster.h"
#include <algorithm>
#include <vector>
#include <map>


#define RD_WIN_L_X 25
#define RD_WIN_R_X 215
#define RD_WIN_SIZ 190

Play_state::Play_state()
	: 
	progress_goal(100000000),
	millis_total(0),
	goal_bar(GOAL_BAR),
	boost_bar(BOOST_BAR),
	boost_bar_remaining(PROGRESS_INDICATOR),
	progress_indicator(PROGRESS_INDICATOR),
	change_state(0),
	enemies()
{

	road[0].setTexture(STAGE02_LARGE);
	road[0].pos.x = RD_WIN_L_X - (road[0].pos.w - RD_WIN_SIZ)/2;

	for (int i = 1; i < 14; ++i)
	{
		road[i] = road[i-1];
		road[i].pos.y += 48;
	}

	goal_bar.pos.x = RD_WIN_L_X + RD_WIN_SIZ;//road[0].pos.w;

	enemies.min_x = road[0].pos.x + 19;
	enemies.max_x = road[0].pos.x + road[0].pos.w - 54;
	player.car_min_x_pos = road[0].pos.x + 19;
	player.car_max_x_pos = road[0].pos.x + road[0].pos.w  - 54;

	boost_bar_remaining.clip.x = 4;
	boost_bar_remaining.clip.y = 0;
	boost_bar_remaining.clip.h = 3;
	boost_bar_remaining.clip.w = 3;
	boost_bar_remaining.pos.x = 7;
	boost_bar_remaining.pos.y = 50;
	boost_bar_remaining.pos.w = 10;
	boost_bar_remaining.pos.h = 250;
	progress_indicator.pos.x = goal_bar.pos.x + 7;
	progress_bar_min = goal_bar.pos.y + goal_bar.pos.h - 27;
	progress_bar_max = progress_bar_min - 215;
	progress_indicator.pos.y = progress_bar_min;
}

void Play_state::process_events(RB_event &evt)
{
	if (evt.type == RB_event::ACTION)
	{
		switch (evt.action)
		{
		case RB_event::UP:
		case RB_event::X:
			player.input[Player_car::UP] = !evt.act_type;
			break;
		case RB_event::Z:
		case RB_event::DOWN:
			player.input[Player_car::DOWN] = !evt.act_type;
			break;
		case RB_event::LEFT:
			player.input[Player_car::LEFT] = !evt.act_type;
			break;
		case RB_event::RIGHT:
			player.input[Player_car::RIGHT] = !evt.act_type;
			break;
		case RB_event::SPACE:
		case RB_event::C:
		{
			player.input[Player_car::BOOST] = !evt.act_type;
			break;
		}
		case RB_event::ESCAPE:
		{
			if (evt.act_type == RB_event::START)
			change_state = 1;
			break;
		}

		}
	}
}

Game_state* Play_state::step(int millis_since_last_step)
{
	timer.step(millis_since_last_step);
	player.step(millis_since_last_step);

	millis_total += millis_since_last_step;
	player.progress += ((player.speed + player.boost) / 20.0f) * millis_since_last_step;

	int hit_speed = enemies.step(millis_since_last_step, player.progress + player.car_y_offset * UNIT_PER_PIXEL,player, player.car_y_offset, player.sprite.pos.x);

	if (hit_speed)
	{
		player.boost_acceleration = 0;
		player.speed = std::max(0,hit_speed);
		player.acceleration = 0;
	}
	if (change_state == 1)
	{
		//HACK: TODO: hackity hack hack
		//find out how to pass ownership to pause_state more gracefully
		Pause_state *temp_ptr = new Pause_state();
		std::unique_ptr<Game_state> new_state{ temp_ptr };
		GAME.current_state.swap(new_state);
		temp_ptr->backdrop.swap(new_state);
		
		//for (bool &b : player.input)
		//{
		//	b = false;
		//}
		change_state = 0;
		return temp_ptr;
	}
	else
	{
		return this;
	}
}

void Play_state::draw(RB_renderer &surf)
{
	int x_off = RD_WIN_SIZ/2  -player.car_x_pos/UNIT_PER_PIXEL;
	x_off = std::max(x_off, -(road[0].pos.w - RD_WIN_SIZ) / 2);
	x_off = std::min(x_off, +(road[0].pos.w - RD_WIN_SIZ) / 2);
	surf.clear();
	for (int i = 0; i < 14; ++i)
	{
		road[i].pos.y = (i - 1) * 48 + ((player.progress) / UNIT_PER_PIXEL + player.car_y_offset) % 48;
		road[i].draw(surf, x_off , 0);
	}
	//RB_sprite dummy = player.car;
	//dummy.pos.x = 130;
	//dummy.pos.y = -500 + ((progress) / UNIT_PER_PIXEL + player.car_y_offset);
	//dummy.draw(surf);

	player.draw(surf, x_off, 0);
	enemies.draw(surf, x_off, 0);

	progress_indicator.pos.y = progress_bar_max + (progress_bar_min - progress_bar_max) * (1.0f - (player.progress*1.0f / progress_goal));
	progress_indicator.draw(surf);
	goal_bar.draw(surf);
	boost_bar.draw(surf);
	boost_bar_remaining.pos.h = 250 * player.boost_remaining / player.boost_full_at;
	boost_bar_remaining.pos.y = 60 + 250 - boost_bar_remaining.pos.h;
	boost_bar_remaining.draw(surf);
	
	timer.draw(surf);

}
Play_state::~Play_state(){}

