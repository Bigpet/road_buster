#pragma once

struct Game_state;


struct RB_event
{
	enum RB_type
	{
		ACTION = 0,
	};

	enum RB_action
	{
		UP = 0,
		DOWN,
		LEFT,
		RIGHT,
		RETURN,
		SPACE,
		C,
		ESCAPE,
		Z,
		X
	};
	enum RB_action_type
	{
		START = 0,
		STOP
	};

	RB_type type;
	RB_action action;
	RB_action_type act_type;
	static void translate_and_forward_event(void *evt, Game_state& state);
	static void translate_and_forward_event(void *evt);
};

