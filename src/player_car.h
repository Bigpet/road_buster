#pragma once

#include "rb_sprite.h"

struct Player_car
{

	enum actions
	{
		UP = 0, DOWN, LEFT, RIGHT, BOOST, ESCAPE
	};

	Player_car();
	int max_speed;
	int acceleration;
	int max_acceleration;
	int boost;
	int boost_remaining;
	int boost_full_at;
	int boost_acceleration;
	int car_y_offset;
	int progress;
	int speed;
	int car_x_pos;
	int car_min_x_pos;
	int car_max_x_pos;
	bool input[5]; //up, down, left, right, boost
	int millis_total;

	RB_sprite sprite;
	void draw(RB_renderer &surf, int x_off = 0, int y_off = 0);
	void step(int millis_since_last_step);
	int getYOff();
};