#include "level_timer.h"

#include <vector>

std::vector<RB_rect> numbers = { {0,0,9,12},  };


Level_timer::Level_timer()
	: millis_total(0),
	number_sprites(NUMBERS),
	millis_to_wait(60 * 1000)
{

}


void Level_timer::draw(RB_renderer &surf)
{
	int num_to_display = 0;
	if (millis_to_wait > millis_total)
	{
		num_to_display = (millis_to_wait - millis_total) / 1000;
	}

	int digit2 = num_to_display % 10;
	int digit1 = ((num_to_display % 100) - digit2)/10;

	RB_rect digit1_rect{ digit1 * 9, 0, 9, 12 };
	RB_rect digit2_rect{ digit2 * 9, 0, 9, 12 };
	number_sprites.pos = { 224, 25, 9, 12 };
	number_sprites.clip = digit1_rect;
	number_sprites.draw(surf);
	number_sprites.pos = { 224, 40, 9, 12 };
	number_sprites.clip = digit2_rect;
	number_sprites.draw(surf);
}

int Level_timer::step(int millis_since_last_step)
{
	millis_total += millis_since_last_step;
	return 0;
}
