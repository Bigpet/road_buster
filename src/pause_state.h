#pragma once
#include "game_state.h"
#include "rb_sprite.h"
#include <memory>

struct Pause_state : Game_state
{
	Pause_state();

	void process_events(RB_event &evt);
	Game_state* step(int millis_since_last_step);
	void draw(RB_renderer &surf);

	std::unique_ptr<Game_state> backdrop;
	bool forward_backdrop;
	RB_sprite pause;
	bool exit_next;
};