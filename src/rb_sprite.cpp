#include "rb_sprite.h"


RB_sprite::RB_sprite() : pos(), clip()
{
	pos.h = 0;

}

RB_sprite::RB_sprite(enTexture ptex) : 
tex(new RB_texture(ptex)),
clip()
{
	pos.x = 0;
	pos.y = 0;
	pos.w = tex->w;
	pos.h = tex->h;
}

void RB_sprite::setTexture(enTexture ptex)
{
	tex.reset(new RB_texture(ptex));
	pos.w = tex->w;
	pos.h = tex->h;

}


void RB_sprite::draw(RB_renderer &rnd, int x_off, int y_off)
{
	pos.x += x_off;
	pos.y += y_off;
	draw(rnd);
	pos.x -= x_off;
	pos.y -= y_off;
}

void RB_sprite::draw(RB_renderer &ren)
{
	if (clip.w == 0 && clip.h == 0)
	{
		ren.draw_texture(*tex, nullptr, &pos);
	}
	else
	{
		ren.draw_texture(*tex, &clip, &pos);
	}
}