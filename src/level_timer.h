#pragma once

#include "rb_sprite.h"

struct Level_timer
{
	Level_timer();
	void draw(RB_renderer &surf);
	int step(int millis_since_last_step);

	RB_sprite number_sprites;
	int millis_total;
	int millis_to_wait;
};