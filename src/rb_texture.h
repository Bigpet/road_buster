#pragma once

struct SDL_Texture;
struct SDL_Surface;

enum enTexture
{
	BACKGROUND,
	ARROW,
	ICON,
	PLAYER_CAR,
	NPC_CARS01,
	STAGE02,
	STAGE02_LARGE,
	BOOST_BAR,
	GOAL_BAR,
	PROGRESS_INDICATOR,
	NUMBERS,
	PAUSE_TEX

};

struct RB_texture
{
	struct RB_texstate;
	int w, h;
	bool has_alpha;
	RB_texstate *state;
	RB_texture(enTexture);
	~RB_texture();

	void *get_internal_store();
	RB_texture(const RB_texture &other) = delete;
	static void set_icon();
};


