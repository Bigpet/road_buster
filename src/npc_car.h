#pragma once 

#include "rb_sprite.h"
//TODO(Peter): remove this, only one define should be done
#define UNIT_PER_PIXEL 1600

struct RB_renderer;
struct Player_car;

struct Npc_car
{

	Npc_car();
	void draw(RB_renderer &surf, int x_off = 0, int y_off = 0);
	void generate_cars();
	int step(int millis_since_last_step, int pos, Player_car& player_car, int posy, int posx);
	Npc_car *get_random_car();

	RB_sprite cars;

	int min_x;
	int max_x;

	int generated_up_to;
	int millis_total;
	int progress;
};