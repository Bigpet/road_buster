#pragma once

#include "game_state.h"
#include "rb_texture.h"
#include "rb_sprite.h"

//TODO: (Peter): remove this forward decl

struct Main_menu_state : public Game_state
{
	//TODO: (PEter): remove this temp var:
	RB_texture bg_tex;
	RB_sprite arrow_tex;
	int selection;
	int num_entries;
	int entry_offset;

	int switch_state;

	Main_menu_state();
	void process_events(RB_event &evt);
	Game_state* step(int millis_since_last_step);
	void draw(RB_renderer &surf);
	~Main_menu_state();
};