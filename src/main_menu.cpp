#include <string>
	
#include "Main_menu.h"
#include "rb_renderer.h"
#include "rb_texture.h"
#include "rb_event.h"
#include "road_buster.h"
#include "play_state.h"

#include <cmath>

enum Menu_Selection
{
	MENU_START_GAME = 0,
	MENU_OPTIONS = 1,
	MENU_CREDITS = 2
};

Main_menu_state::Main_menu_state()
	: selection(0),
	num_entries(3),
	entry_offset(32),
	bg_tex(BACKGROUND),
	arrow_tex(ARROW)
{
	arrow_tex.pos.x = 15;
	arrow_tex.pos.y = 175;
	arrow_tex.pos.w = 32;
	arrow_tex.pos.h = 32;

	switch_state = 0;
}

void Main_menu_state::process_events(RB_event &evt)
{
	if (evt.type == RB_event::ACTION)
	{
		if (evt.act_type == RB_event::START)
		{
			switch (evt.action)
			{
			case RB_event::DOWN:
				selection = (selection + 1) % num_entries;
				break;
			case RB_event::UP:
				selection = (selection + num_entries - 1) % num_entries;
				break;
			case RB_event::RETURN:
			{
				if (selection == MENU_START_GAME)
				{
					switch_state = 1;
				}
				break;
			}
			}
		}
	}

}

Game_state* Main_menu_state::step(int millis_since_last_step)
{
	
	if (switch_state == 1)
	{
		return new Play_state();
	}
	else
	{
		return this;
	}
}
void Main_menu_state::draw(RB_renderer &renderer){
	RB_sprite menu_arrow = arrow_tex;

	menu_arrow.pos.y += selection * entry_offset;

	renderer.clear();
	renderer.draw_texture(bg_tex,NULL,NULL);
	menu_arrow.draw(renderer);

}

Main_menu_state::~Main_menu_state()
{
}
