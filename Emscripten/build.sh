#!/bin/bash

#TEMPORARY BUILD SCRIPT TO BE REPLACED WITH CMAKE FILE
#this needs to executed with as ./Emscripten/build.sh

#em++ -I../../SDL2/include unity.cpp ../../SDL2/build/.libs/libSDL2.a -o test.html -lSDL2
#em++ unity.cpp -o test.html -s USE_SDL=2 -s LEGACY_GL_EMULATION=1
em++ -O2 -c Emscripten/unity.cpp -o road_buster.o
emcc -O2 -minify --memory-init-file 1  road_buster.o -s USE_SDL=2 -s LEGACY_GL_EMULATION=1 -o Emscripten/test.html --preload-file res/img/arrow.png --preload-file res/img/background.png --preload-file res/img/icon.png
cp -r ./res ./Emscripten/
