
//add include directory ../src

#include "../SDL/rb_platform_SDL.cpp"
#include "../SDL/rb_event_SDL.cpp"
#include "../SDL/rb_renderer_SDL.cpp"
#include "../SDL/rb_texture_SDL.cpp"
#include "../SDL/rb_globals_SDL.cpp"

#include "../src/game_state.cpp"
#include "../src/main_menu.cpp"
#include "../src/play_state.cpp"
#include "../src/player_car.cpp"
#include "../src/npc_car.cpp"
#include "../src/rb_sprite.cpp"
#include "../src/road_buster.cpp"
#include "../src/level_timer.cpp"
#include "../src/pause_state.cpp"
#include "../src/main.cpp"
