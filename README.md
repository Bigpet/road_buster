# README #

This is a C++ SDL2 implementation of a game idea from http://gamedanteam.com/board/23/road-buster

### How do I get set up? ###

I only provide a project file for VS2013 for now, but I'm not heavily depending on it and will add a CMake Makefile later (to test on Linux).

For now, 

* install VS2013
* download the SDL2 development files (including headers, *.lib and *.dll)
* open `Win32/LibraryFolders.props` in your favorite text editor
* change the folder references to point to where you saved SDL2 
* open the `Win32/road_buster.sln` and press build