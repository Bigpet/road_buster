#pragma once 

#include "SDL.h"
#include <string>
struct RB_renderer;

struct SDL_Globals
{
	SDL_Window *wnd;
	SDL_Renderer *ren;
	std::string game_path;
	RB_renderer *wrap_ren;
	bool should_quit;
};

extern SDL_Globals G;
