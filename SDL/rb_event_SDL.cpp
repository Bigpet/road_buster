#include "../src/rb_event.h"
#include "../src/road_buster.h"
#include "../src/game_state.h"
#include "SDL_events.h"

void RB_event::translate_and_forward_event(void *pevt)
{
	RB_event::translate_and_forward_event(pevt, *GAME.current_state);
}
void RB_event::translate_and_forward_event(void *pevt, Game_state& state)
{
	SDL_Event *evt = (SDL_Event *) pevt;

	RB_event translation;
	bool forward = false;

	switch (evt->type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
	{
		translation.type = ACTION;
		translation.act_type = (evt->type == SDL_KEYDOWN ? START : STOP);
		SDL_KeyboardEvent *key_evt = (SDL_KeyboardEvent *) evt;
		switch (key_evt->keysym.sym)
		{
		case SDLK_UP:
			translation.action = UP;
			forward = true;
			break;
		case SDLK_DOWN:
			translation.action = DOWN;
			forward = true;
			break;
		case SDLK_LEFT:
			translation.action = LEFT;
			forward = true;
			break;
		case SDLK_RIGHT:
			translation.action = RIGHT;
			forward = true;
			break;
		case SDLK_SPACE:
			translation.action = SPACE;
			forward = true;
			break;
		case SDLK_c:
			translation.action = C;
			forward = true;
			break;
		case SDLK_x:
			translation.action = X;
			forward = true;
			break;
		case SDLK_z:
		case SDLK_y:
			translation.action = Z;
			forward = true;
			break;
		case SDLK_RETURN:
			translation.action = RETURN;
			forward = true;
			break;
		case SDLK_ESCAPE:
			translation.action = ESCAPE;
			forward = true;
			break;
		}
	}
		break;
	}

	if(forward)
	{
		state.process_events(translation);
	}
}
