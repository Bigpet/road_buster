#include "../src/rb_texture.h"
#include "../src/road_buster.h"
#include "../src/rb_platform.h"

#include "rb_globals_SDL.h"

#include "SDL.h"

///////////////////////////////////////////////////////////////////////////
//struct RB_texture
//{
//	struct RB_texstate;
//	int w, h;
//	bool has_alpha;
//	RB_texstate *state;
//	RB_texture(enTexture);
//	RB_texture(const RB_texture &other) = delete;
//	static void set_icon();
//private:
//	static void loadSurface(enTexture choice, SDL_Surface *&out_surf, unsigned char *&out_stbi);
//};

#define STBI_NO_HDR
#define STB_IMAGE_IMPLEMENTATION 1
#include "../src/ext/stb_image.h"

#include <string>

//TODO: (Peter): get proper resource handling going
#ifdef _WIN32
#define RES_ROOT SDL_GetBasePath()
#define RESSOURCE_FOLDER "res\\img\\"
#else
#define RES_ROOT "./"
#define RESSOURCE_FOLDER "res/img/"
#endif

#define BACKGROUND_GIF "background.gif"
#define ARROW_GIF "arrow.gif"
#define ICON_GIF "icon.gif"
#define CAR00_GIF "Car00.gif"
#define STAGE02_GIF "Stage02.gif"
#define STAGE02_LARGE_GIF "Stage02_large.gif"
#define BOOST_BAR_GIF "Boost_bar.gif"
#define GOAL_BAR_GIF "Goal_bar.gif"
#define CARS01_GIF "Cars01.gif"
#define NUMBERS_GIF "numbers.gif"
#define PAUSE_GIF "Pause.gif"


#define PROGRESS_INDICATOR_GIF "progress_indicator.gif"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
uint32_t g_rmask = 0xff000000;
uint32_t g_gmask = 0x00ff0000;
uint32_t g_bmask = 0x0000ff00;
uint32_t g_amask = 0x000000ff;
#else
uint32_t g_rmask = 0x000000ff;
uint32_t g_gmask = 0x0000ff00;
uint32_t g_bmask = 0x00ff0000;
uint32_t g_amask = 0xff000000;
#endif

struct RB_texture::RB_texstate
{
	SDL_Texture *img_data;
	static void loadSurface(enTexture choice, SDL_Surface *&out_surf, unsigned char *&out_stbi);

};

void make_color_transparent(int r, int g, int b, SDL_Surface *&tex)
{
	if (!tex->format->Amask)
	{
		SDL_Surface *temp = SDL_ConvertSurfaceFormat(tex, SDL_PIXELFORMAT_ABGR8888, 0);
		SDL_FreeSurface(tex);
		tex = temp;
	}
	else if (tex->format->BitsPerPixel != 32)
	{
		show_fatal_error("NOT A 32 BIT IMAGE");
	}

	SDL_LockSurface(tex);

	char *pixels = (char *) tex->pixels;
	char byte_per_pixel = tex->format->BytesPerPixel;

	for (int y = 0; y < tex->h; ++y)
	{
		for (int x = 0; x < tex->w; ++x)
		{
			uint8_t pr, pg, pb;

			uint32_t *colors = (uint32_t *) &pixels[tex->pitch*y + byte_per_pixel *x];
			SDL_GetRGB(*colors, tex->format, &pr, &pg, &pb);
			if (r == pr
				&& g == pg
				&& b == pb)
			{
				*colors = *colors & ~tex->format->Amask;
			}
		}
	}

	SDL_UnlockSurface(tex);
}

RB_texture::RB_texture(enTexture choice)
{
	state = new RB_texstate();
	unsigned char *img_data;
	SDL_Surface * bmp;
	RB_texstate::loadSurface(choice, bmp, img_data);


	SDL_Texture *tex = SDL_CreateTextureFromSurface(G.ren, bmp);
	w = bmp->w;
	h = bmp->h;
	
	has_alpha = bmp->format->Amask;
	if (bmp->format->Amask)
	{
		SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_BLEND);
	}
	else
	{
		SDL_SetTextureBlendMode(tex, SDL_BLENDMODE_NONE);
	}
	SDL_FreeSurface(bmp);
	stbi_image_free(img_data);
	if (tex == nullptr){
		std::string err("SDL_CreateTextureFromSurface Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
		G.should_quit = true;
		//TODO: (Peter): find out why emscripten doesn't like exceptions
		//throw std::exception("SURF TO TEX UNSUCCESSFUL");
	}
	state->img_data = tex;
}

RB_texture::~RB_texture()
{
	SDL_DestroyTexture(state->img_data);
	delete state;
}


void *RB_texture::get_internal_store()
{
	return (void *)state->img_data;
}


//TODO: (Peter): having to delete parameters after calling this is not very safe or friendly
void RB_texture::RB_texstate::loadSurface(enTexture choice, SDL_Surface *&out_surf, unsigned char *&out_stbi)
{
	std::string tex_path = RES_ROOT; tex_path.append(RESSOURCE_FOLDER);

	switch (choice)
	{
	case BACKGROUND:
		tex_path.append(BACKGROUND_GIF);
		break;
	case ARROW:
		tex_path.append(ARROW_GIF);
		break;
	case ICON:
		tex_path.append(ICON_GIF);
		break;
	case PLAYER_CAR:
		tex_path.append(CAR00_GIF);
		break;
	case NPC_CARS01:
		tex_path.append(CARS01_GIF);
		break;
	case STAGE02:
		tex_path.append(STAGE02_GIF);
		break;
	case BOOST_BAR:
		tex_path.append(BOOST_BAR_GIF);
		break;
	case GOAL_BAR:
		tex_path.append(GOAL_BAR_GIF);
		break;
	case PROGRESS_INDICATOR:
		tex_path.append(PROGRESS_INDICATOR_GIF);
		break;
	case NUMBERS:
		tex_path.append(NUMBERS_GIF);
		break;
	case PAUSE_TEX:
		tex_path.append(PAUSE_GIF);
		break;
	case STAGE02_LARGE:
		tex_path.append(STAGE02_LARGE_GIF);
	default:
		//TODO: (Peter): seriously evaluate the use of exceptions
		//TODO: (Peter): find out why emscripten doesn't like exceptions
		//throw std::exception("UNKNOWN TEXTURE REQUESTED");
		break;
	}


	int width, height, depth;
	printf("openening %s \n", tex_path.c_str());
	out_stbi = stbi_load(tex_path.c_str(), &width, &height, &depth, 0);
	if (out_stbi == nullptr)
	{
		show_fatal_error(stbi_failure_reason());
	}

	out_surf = SDL_CreateRGBSurfaceFrom(out_stbi,
		width,
		height,
		depth * 8,
		width * depth,
		g_rmask,
		g_gmask,
		g_bmask,
		(depth == 3) ? 0 : g_amask);

	//SDL_Surface *bmp = SDL_LoadBMP(tex_path.c_str());
	if (out_surf == nullptr){
		std::string err("SDL_CreateRGBSurfaceFrom Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
		G.should_quit = true;
		//TODO: (Peter): find out why emscripten doesn't like exceptions
		//throw std::exception("TEXTURE LOAD UNSUCCESSFUL");
	}
}

void RB_texture::set_icon()
{
	SDL_Surface *surf;
	unsigned char*img_data;
	RB_texstate::loadSurface(ICON, surf, img_data);

	SDL_SetWindowIcon(G.wnd, surf);

	SDL_FreeSurface(surf);
	stbi_image_free(img_data);

}

