#include "../src/rb_renderer.h"
#include "../src/rb_platform.h"
#include "../src/rb_texture.h"

#include "SDL.h"
#include "rb_globals_SDL.h"

RB_renderer *get_renderer()
{
	return G.wrap_ren;
}


void RB_renderer::clear()
{
	//TODO: (Peter): consider saving the renderer in "state"
	SDL_RenderClear(G.ren);
}

void RB_renderer::draw_texture(RB_texture &tex, RB_rect *source, RB_rect *target)
{
	//TODO: (Peter): don't rely on binary identity of SDL_Rect and RB_rect
	SDL_Rect sdl_target;
	if (target)
	{
		sdl_target.x = target->x*2;
		sdl_target.y = target->y*2;
		sdl_target.w = target->w*2;
		sdl_target.h = target->h*2;
		SDL_RenderCopy(G.ren, (SDL_Texture *) tex.get_internal_store(), (SDL_Rect *) source, &sdl_target);
	}
	else
	{
		SDL_RenderCopy(G.ren, (SDL_Texture *) tex.get_internal_store(), (SDL_Rect *) source, (SDL_Rect *) target);
	}
}

void RB_renderer::present()
{
	SDL_RenderPresent(G.ren);
}
