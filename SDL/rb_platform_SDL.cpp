#include "../src/rb_platform.h"
#include "../src/rb_texture.h"
#include "../src/rb_event.h"
#include "../src/rb_renderer.h"

#include <algorithm>
#include <cstdio>
#include "SDL.h"

#include "rb_globals_SDL.h"


static Uint8 *audio_pos; // global pointer to the audio buffer to be played
static Uint32 audio_len; 
static Uint32 audio_len_remaining; // remaining length of the sample we have to play
void my_audio_callback(void *userdata, Uint8 *stream, int len) {

	if (audio_len_remaining == 0)
	{
		audio_pos -=audio_len-44100;
		audio_len_remaining = audio_len-44100;
	}


	int lenl = (len > audio_len_remaining ? audio_len_remaining : len);

	if (audio_len_remaining - lenl < 88100)
	{
		SDL_memset(stream, 0, lenl);

		int audio_len_temp = audio_len_remaining;
		int len_temp = lenl;
		int offset = 0;
		
		while (len_temp>0)
		{
			int chunk = len_temp < 1000 ? len_temp : 1000;
			if (audio_len_remaining - offset < 88100)
			{
				float fade = (audio_len_remaining - offset) / 88100.0f;
				SDL_MixAudio(stream +offset, audio_pos + offset, chunk, (SDL_MIX_MAXVOLUME/4) * fade);
				SDL_MixAudio(stream + offset, (audio_pos + offset - audio_len + 88100), chunk, (SDL_MIX_MAXVOLUME/4) * (1 - fade));
			}
			else
			{
				SDL_MixAudio(stream + offset, audio_pos + offset, chunk, SDL_MIX_MAXVOLUME/4);
			}

			len_temp -= chunk;
			offset += chunk;
		}
		//SDL_memcpy(temp_mem, audio_pos, lenl); 					// simply copy from one buffer into the other
		//SDL_memcpy(stream, temp_mem, lenl); 					// simply copy from one buffer into the other
		//delete [] temp_mem;
	}
	else
	{
		SDL_memset(stream, 0, lenl);
		SDL_MixAudio(stream, audio_pos, lenl, SDL_MIX_MAXVOLUME / 4);
		//SDL_memcpy(stream, audio_pos, lenl); 					// simply copy from one buffer into the other
	}


	//SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);// mix from one buffer into another
	if (lenl < len)
	{
		audio_pos += lenl;
		audio_len_remaining -= lenl;
		my_audio_callback(userdata,stream+lenl,len-lenl);
	}
	else
	{
		audio_pos += len;
		audio_len_remaining -= len;
	}
}



//Initialization and shutdown
struct RBp_settings
{
	const char * APPLICATION = "ROAD BUSTER";
	const int DEFAULT_WIDTH = 480;
	const int DEFAULT_HEIGHT = 640;
};

RBp_settings *create_Settings()
{
	return new RBp_settings();
}

void readSettings(RBp_settings *settings)
{

}

void init(RBp_settings *settings)
{
	G.wnd = nullptr;
	G.ren = nullptr;

	//SDL_Init
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0){
		std::string err("SDL_Init Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
	}

	//SDL create winow
	G.wnd = SDL_CreateWindow(
		settings->APPLICATION,
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		settings->DEFAULT_WIDTH, settings->DEFAULT_HEIGHT,
		SDL_WINDOW_SHOWN);
	if (G.wnd == nullptr){
		std::string err("SDL_CreateWindow Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
	}

	//SDL create renderer
	//TODO: (Peter): read setting for video device from config file
	G.ren = SDL_CreateRenderer(G.wnd, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (G.ren == nullptr){
		std::string err("SDL_CreateRenderer Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
		SDL_DestroyWindow(G.wnd);
		SDL_Quit();
	}


	//SDL_GetBasePath
#ifdef __EMSCRIPTEN__
	G.game_path = std::string("");
#else
	char *temp_path = SDL_GetBasePath();
	if (temp_path == nullptr){
		std::string err("SDL_GetBasePath Error: ");
		err.append(SDL_GetError());
		show_fatal_error(err);
	}
	G.game_path = std::string(temp_path);
	SDL_free(temp_path);
#endif
	RB_texture::set_icon();

	G.wrap_ren = new RB_renderer();

	G.should_quit = false;

//	///////////////////////////audio test
//	// local variables
//	static Uint32 wav_length; // length of our sample
//	static Uint8 *wav_buffer; // buffer containing our audio file
//	static SDL_AudioSpec wav_spec; // the specs of our piece of music
//
//
//	/* Load the WAV */
//	// the specs, length and buffer of our wav are filled
//#ifdef _WIN32
//	static std::string music_path = G.game_path + "res\\music\\Belle.wav";
//#else
//	static std::string music_path = G.game_path + "res/music/Belle.wav";
//#endif
//	if (SDL_LoadWAV(music_path.c_str(), &wav_spec, &wav_buffer, &wav_length) == NULL){
//		wav_length = 5;
//	}
//	// set the callback function
//	wav_spec.callback = my_audio_callback;
//	wav_spec.userdata = wav_buffer;
//	// set our global static variables
//	audio_pos = wav_buffer; // copy sound buffer
//	audio_len = wav_length; // copy file length
//	audio_len_remaining = audio_len;
//	/* Open the audio device */
//	if (SDL_OpenAudio(&wav_spec, NULL) < 0){
//		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
//	}
//
//	/* Start playing */
//	SDL_PauseAudio(0);
//	//////////////////////////////////////
}

void shutdown(RBp_settings *settings)
{
	SDL_DestroyRenderer(G.ren);
	SDL_DestroyWindow(G.wnd);
}


//Utility 
void show_fatal_error(const std::string &msg)
{
#ifdef __EMSCRIPTEN__
	printf("ERROR: %s\n",msg.c_str());
#else
		SDL_MessageBoxButtonData button_data;
		SDL_MessageBoxData data;

		button_data.buttonid = 0;
		button_data.flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;
		button_data.text = "OK";

		data.flags = SDL_MESSAGEBOX_ERROR;
		data.window = G.wnd;
		data.title = "FATAL ERROR";
		data.message = msg.c_str();
		data.numbuttons = 1;
		data.buttons = &button_data;
		data.colorScheme = NULL;

		int buttonid;
		if (SDL_ShowMessageBox(&data, &buttonid) < 0) {
			SDL_Log("error displaying message box, \"%s\"", SDL_GetError());
		}
#endif
}

std::string translate_directory(const std::string &relative_path)
{
	return relative_path;
}

//entry point into loop
void main_loop(std::function<void(void)> callback)
{

	while (!G.should_quit)
	{

		SDL_Event evt;
		while (SDL_PollEvent(&evt))
		{
			switch (evt.type)
			{
			case SDL_QUIT:
				G.should_quit = true;
				return;
				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				RB_event::translate_and_forward_event(&evt);
				break;
			}
		}
		callback();
	}
}
